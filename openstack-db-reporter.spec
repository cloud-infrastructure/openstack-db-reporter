Name:           openstack-db-reporter
Version:        0.0
Release:        2%{?dist}
Summary:        Tool for checking OpenStack usage on terminal
Source0:        %{name}-%{version}.tar.gz
BuildArch:      noarch

Group: CERN/Utilities
License: Apache License, Version 2.0
URL: https://openstack.cern.ch/

BuildRequires: python-devel
Requires: openstack-db-common
Requires: python-prettytable

%description
Tool for checking OpenStack usage on terminal

%prep
%setup -q

%build
CFLAGS="%{optflags}" %{__python} setup.py build

%install
%{__rm} -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}

install -p -D -m 755 scripts/dbreporter %{buildroot}/usr/bin/dbreporter

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)

%{_bindir}/dbreporter

%{python_sitelib}/*

%dir /etc/%{name}
/etc/%{name}/dbreporter.conf.sample

%changelog
* Mon May 04 2015 Wataru Takase <wataru.takase@cern.ch> 0.0-2
- Refactored codes

* Thu Oct 10 2014 Wataru Takase <wataru.takase@cern.ch> 0.0-1
- First version
