#!/usr/bin/env python

from distutils.core import setup

setup(name='openstack-db-reporter',
      version='0.0',
      description='Tool for checking OpenStack usage on terminal',
      author='Cloud Infrastructure Team',
      author_email='cloud-infrastructure-3rd-level@cern.ch',
      url='http://www.cern.ch/openstack',
      package_dir= {'': 'src'},
      packages=['openstack_db_reporter'],
      scripts=['scripts/dbreporter'],
      data_files=[("/etc/openstack-db-reporter",["etc/openstack-db-reporter/dbreporter.conf.sample"])]
     )
