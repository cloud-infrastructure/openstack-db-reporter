import copy
import re


def nested_table(table, subtable, hchar="-", vchar="|", jchar="+"):
    try:
        str_table = table.get_string()
    except:
        str_table = table
    subtable["sep"] = " "
    subtable["max_digit"] = len(max(subtable["label"]))
    dict_table = str_table_to_dict(str_table, subtable, hchar, vchar, jchar)
    return dict_table_to_str(dict_table)[:-1]


def str_table_to_dict(str_table, subtable, hchar, vchar, jchar):
    table = {}
    table["hchar"] = hchar
    table["vchar"] = vchar
    table["jchar"] = jchar
    table["table"] = []
    table["subtable"] = subtable
    table["header"] = []
    table["first_row"] = []
    table["template_row"] = []
    lines = str_table.split("\n")
    table["row_num"] = len(lines)
    row_index = 0
    for line in lines:
        if line.startswith(vchar):
            sep = vchar
        else:
            sep = jchar
        line = line[1:-1]
        row = []
        col_index = 0
        for elm in line.split(sep):
            col = dict()
            if row_index == 1:
                table["header"].append(elm)
            if table["subtable"]["flag"][col_index]:
                col["sep"] = sep + sep
                subcol = {}
                subcol["sep"] = sep
                if row_index < 3 or row_index == table["row_num"] - 1:
                    subcol["value"] = [elm]
                else:
                    subcol["value"] = elm.split(table["subtable"]["sep"])
                    while subcol["value"].count(""):
                        subcol["value"].remove("")
                    for subcol_value in subcol["value"]:
                        subcol_digit = len(subcol_value)
                        if subcol_digit > table["subtable"]["max_digit"]:
                            table["subtable"]["max_digit"] = subcol_digit
            elif row_index == 1:
                col["sep"] = sep
                subcol = {}
                subcol["sep"] = sep
                subcol["value"] = [re.sub(r"\S", " ", elm)]
            elif row_index == 2:
                col["sep"] = table["vchar"]
                subcol = {}
                subcol["sep"] = table["vchar"]
                subcol["value"] = [table["header"][col_index]]
            else:
                col["sep"] = sep
                subcol = {}
                subcol["sep"] = sep
                subcol["value"] = [elm]
            col["value"] = subcol
            row.append(col)
            col_index += 1
        table["table"].append(row)
        if row_index == 0:
            table["first_row"] = copy.deepcopy(row)
        if row_index == 1:
            table["template_row"] = copy.deepcopy(row)
        elif row_index == 2:
            row = copy.deepcopy(table["template_row"])
            col_index = 0
            for col in row:
                if table["subtable"]["flag"][col_index]:
                    col["value"]["value"] = table["subtable"]["label"]
                col_index += 1
            table["table"].append(row)
            table["table"].append(table["first_row"])
        row_index += 1
    table["subtable"]["width"] = table["subtable"]["max_digit"] + 2
    table["subtable"]["width"] *= table["subtable"]["col_num"]
    table["subtable"]["width"] += table["subtable"]["col_num"] - 1
    table["subtable"]["col_width"] = table["subtable"]["width"]
    table["subtable"]["col_width"] /= table["subtable"]["col_num"]
    table["row_num"] = len(table["table"])
    return table


def dict_table_to_str(dict_table):
    hchar = dict_table["hchar"]
    vchar = dict_table["vchar"]
    jchar = dict_table["jchar"]
    table = ""
    row_index = 0
    for row in dict_table["table"]:
        line = ""
        col_index = 0
        presep = ""
        for col in row:
            if col_index > 1 and len(presep) == 2:
                line += presep
            else:
                line += col["sep"]
            subcol = col["value"]
            if dict_table["subtable"]["flag"][col_index]:
                subcol_index = 0
                for subcol_value in subcol["value"]:
                    if subcol_index != 0:
                        line += subcol["sep"]
                    if row_index == 0:
                        subcol_value = ""
                        for i in range(dict_table["subtable"]["col_num"]):
                            subcol_value += "".rjust(
                                dict_table["subtable"]["col_width"], hchar)
                            if i < dict_table["subtable"]["col_num"] - 1:
                                subcol_value += hchar
                    elif (row_index == 2 or row_index == 4 or
                            row_index == dict_table["row_num"] - 1):
                        subcol_value = ""
                        for i in range(dict_table["subtable"]["col_num"]):
                            subcol_value += "".rjust(
                                dict_table["subtable"]["col_width"], hchar)
                            if i < dict_table["subtable"]["col_num"] - 1:
                                subcol_value += jchar
                    elif row_index == 1:
                        padding = dict_table["subtable"]["width"]
                        label = subcol_value.replace(" ", "")
                        padding -= len(label)
                        subcol_value = ""
                        subcol_value += "".rjust(padding / 2)
                        subcol_value += label
                        subcol_value += "".ljust(padding / 2 + padding % 2)
                    else:
                        padding = dict_table["subtable"]["col_width"]
                        value = subcol_value.replace(" ", "")
                        padding -= len(value)
                        subcol_value = ""
                        subcol_value += "".rjust(padding / 2)
                        subcol_value += value
                        subcol_value += "".ljust(padding / 2 + padding % 2)
                    line += subcol_value
                    subcol_index += 1
            else:
                line += subcol["value"][0]
            col_index += 1
            presep = col["sep"]
        line += presep
        table += line + "\n"
        row_index += 1
    table = table.replace(" %s%s" % (jchar, jchar), " %s%s" % (vchar, jchar))
    table = table.replace("%s%s " % (jchar, jchar), "%s%s " % (jchar, vchar))
    table = table.replace("%s%s" % (hchar, vchar), "%s%s" % (hchar, jchar))
    table = table.replace("%s%s" % (vchar, hchar), "%s%s" % (jchar, hchar))
    return table
