import collections
import copy
import prettytable

import utils
from base import BaseTable


class UsageMixin(object):

    def __init__(self):
        pass

    def _usage_percent(self, quota, used):
        if int(quota) > 0:
            return int(float(used) / float(quota) * 100)
        else:
            return "-"

    def _subtable_meta(self, flag):
        subtable = {}
        subtable["flag"] = flag
        subtable["col_num"] = 3
        subtable["label"] = ["Used", "Quota", "Used %"]
        subtable["sep"] = " "
        subtable["max_digit"] = len(max(subtable["label"]))
        return subtable


class CellsTable(BaseTable, UsageMixin):

    def __init__(self, nova_top, cells):
        self.nova_top = nova_top
        self.cells = cells
        self.top_info = nova_top.init_cell_info()
        self.ram_frees = []
        fields = ["Cell", "Active Nodes", "Nodes", "Cores",
                  "RAM (GB)", "Disk (TB)", "QEMU", "Hyper-V",
                  "Active VMs", "Error VMs", "VMs"]
        super(CellsTable, self).__init__(fields=fields)

    def _add_row(self, cell_name, info, vms):
        self.table.add_row([
            cell_name,
            info["servicenodes"]["active"], info["servicenodes"]["total"],
            "%s %s %s" % (info["vcpus_used"],
                          info["vcpus"],
                          self._usage_percent(info["vcpus"],
                                                  info["vcpus_used"])),
            "%s %s %s" % (int(info["ram_used"] / 1024),
                          int(info["ram"] / 1024),
                          self._usage_percent(info["ram"],
                                                  info["ram_used"])),
            "%s %s %s" % (int(info["disk_used"] / 1024),
                          int(info["disk"] / 1024),
                          self._usage_percent(info["disk"],
                                                  info["disk_used"])),
            info["hypervisors"]["QEMU"], info["hypervisors"]["hyperv"],
            vms["active"], vms["error"], info["running_vms"]])

    def _add_cell_info(self, cell_info):
        self.top_info["up"] += cell_info["up"]
        self.top_info["down"] += cell_info["down"]
        self.top_info["vcpus"] += cell_info["vcpus"]
        self.top_info["ram"] += cell_info["ram"]
        self.top_info["disk"] += cell_info["disk"]
        self.top_info["vcpus_used"] += cell_info["vcpus_used"]
        self.top_info["ram_used"] += cell_info["ram_used"]
        self.top_info["disk_used"] += cell_info["disk_used"]
        self.top_info["running_vms"] += cell_info["running_vms"]
        for flavor in cell_info["ram_flavors"]:
            self.top_info["ram_free"][flavor] += cell_info["ram_free"][flavor]
        for key, value in cell_info["hypervisors"].items():
            if key not in self.top_info["hypervisors"]:
                self.top_info["hypervisors"][key] = 0
            self.top_info["hypervisors"][key] += value
        for key, value in cell_info["servicenodes"].items():
            self.top_info["servicenodes"][key] += value

    def _create(self):
        for cell in self.cells:
            cell_info = cell.cell_info()
            self._add_cell_info(cell_info)
            vms = cell.vm_num()
            self._add_row(cell.cell, cell_info, vms)
            self.ram_frees.append([cell.cell] + cell_info["ram_free"].values())
        total_vms = self.nova_top.vm_num()
        self._add_row("Total", self.top_info, total_vms)
        self.ram_frees.append(["Total"] + self.top_info["ram_free"].values())
        self.ram_flavors = self.top_info["ram_flavors"]

    def output(self):
        flag = [0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]
        self._output(utils.nested_table(self.table, self._subtable_meta(flag)))


class RamFreesTable(BaseTable):

    def __init__(self, ram_flavors=[], ram_frees=[]):
        self.ram_frees = ram_frees
        fields = ["Instance Free"]
        for ram_flavor in ram_flavors:
            if ram_flavor == "512":
                fields.append("512 MB")
            else:
                fields.append("%s GB" % int(int(ram_flavor) / 1000))
        super(RamFreesTable, self).__init__(fields=fields)

    def _create(self):
        for ram_free in self.ram_frees:
            self.table.add_row(ram_free)


class FlavorsTable(BaseTable):

    def __init__(self, nova):
        self.nova = nova
        fields = ["Flavor Name", "Number of Instances"]
        super(FlavorsTable, self).__init__(fields=fields)

    def _create(self):
        for flavor in self.nova.flavor_vm_num():
            self.table.add_row([flavor["name"], flavor["number"]])
        self.table.get_string(sortby="Flavor Name")


class ImagesTable(BaseTable):

    def __init__(self, glance):
        self.glance = glance
        fields = ["", "Active", "Snapshots", "Total", "Size (GB)"]
        super(ImagesTable, self).__init__(fields=fields)

    def _create(self):
        active_images = self.glance.active_image_num()
        snapshots = self.glance.snapshot_num()
        size_total = int(self.glance.used_image_size())
        total_images = active_images + snapshots
        self.table.add_row(["Image", active_images, snapshots,
                            total_images, size_total])


class UsersTenantsTable(BaseTable):

    def __init__(self, keystone):
        self.keystone = keystone
        fields = ["", "Total"]
        super(UsersTenantsTable, self).__init__(fields=fields)

    def _create(self):
        users = self.keystone.user_list()
        tenants = self.keystone.tenant_list()
        self.table.add_row(["Tenants", len(tenants)])
        self.table.add_row(["Accounts", len(users)])


class VolumesTable(BaseTable):

    def __init__(self, cinder):
        self.cinder = cinder
        fields = ["Volumes state", "Volumes", "Volume Size (GB)"]
        super(VolumesTable, self).__init__(fields=fields)

    def _create(self):
        total_num = 0
        total_size = 0
        for status, usage in self.cinder.volume_usage_per_status().items():
            self.table.add_row([status.title(), usage["num"], usage["size"]])
            total_num += usage["num"]
            total_size += usage["size"]
        self.table.add_row(["Total", total_num, total_size])


class VolumeTypesTable(BaseTable):

    def __init__(self, cinder):
        self.cinder = cinder
        fields = ["Volume Type", "Volumes", "Volume Size (GB)"]
        super(VolumeTypesTable, self).__init__(fields=fields)

    def _create(self):
        total_num = 0
        total_size = 0
        for type, usage in self.cinder.volume_usage_per_type().items():
            self.table.add_row([type, usage["num"], usage["size"]])
            total_num += usage["num"]
            total_size += usage["size"]
        self.table.add_row(["Total", total_num, total_size])


class TenantUsagesTables(BaseTable, UsageMixin):

    def __init__(self, nova, cinder, keystone):
        self.nova = nova
        self.cinder = cinder
        self.keystone = keystone
        labels = (("domain", "Domain"),
                  ("shared", "Shared Tenant"),
                  ("personal", "Personal Tenant"))
        self.labels = collections.OrderedDict(labels)
        fields = ["Instances", "CPUs", "RAM (GB)", "Volumes",
                  "Snapshots", "Volumes Size (GB)"]
        super(TenantUsagesTables, self).__init__(fields=fields)

    def _init(self):
        self.table = collections.OrderedDict()
        for key, label in self.labels.items():
            table = prettytable.PrettyTable([label]+self.fields)
            table.align[label] = "l"
            table.padding_width = 1
            if key == "shared":
                self.table[key] = {"template": table, "tables": []}
            else:
                self.table[key] = table

    def _tenant_usages(self):
        tenant_list = self.keystone.tenant_list()
        tenants = {}
        domains = {}
        nova_usages = self.nova.tenant_usages(tenant_list)
        resource_keys = ["instances", "cores", "ram"]
        for id, tenant in nova_usages.items():
            name = tenant["name"]
            prefix = tenant["prefix"]
            usages = tenant["usages"]
            tenants[id] = {}
            tenants[id]["name"] = name
            tenants[id]["domain"] = prefix
            if prefix not in domains:
                domains[prefix] = {"name": prefix}
            for k, v in usages.items():
                quota = k
                used = "%s_used" % k
                if quota not in resource_keys:
                    continue

                if quota not in domains[prefix]:
                    domains[prefix][quota] = 0
                    domains[prefix][used] = 0
                domains[prefix][quota] += v["hard_limit"]
                domains[prefix][used] += v["in_use"]

                tenants[id][quota] = v["hard_limit"]
                tenants[id][used] = v["in_use"]

        cinder_usages = self.cinder.tenant_usages(tenant_list)
        resource_keys = ["volumes", "snapshots", "gigabytes"]
        for id, tenant in cinder_usages.items():
            name = tenant["name"]
            prefix = tenant["prefix"]
            usages = tenant["usages"]
            if id not in tenants:
                tenants[id] = {}
                tenants[id]["name"] = name
                tenants[id]["domain"] = prefix
            if prefix not in domains:
                domains[prefix] = {"name": prefix}
            for k, v in usages.items():
                quota = k
                used = "%s_used" % k
                if quota not in resource_keys:
                    continue

                if quota not in domains[prefix]:
                    domains[prefix][quota] = 0
                    domains[prefix][used] = 0
                domains[prefix][quota] += v["hard_limit"]
                domains[prefix][used] += v["in_use"]

                tenants[id][quota] = v["hard_limit"]
                tenants[id][used] = v["in_use"]

        return tenants, domains

    def _add_row(self, table, usage):
        row = [usage["name"]]
        nova_rescs = ["instances", "cores", "ram"]
        for resc in nova_rescs:
            if resc not in usage:
                row.append("- - -")
                continue
            quota = usage[resc]
            used = usage["%s_used" % resc]
            if resc == "ram":
                quota = int(quota / 1024)
                used = int(used / 1024)
            row.append("%s %s %s" % (used, quota,
                                     self._usage_percent(quota, used)))

        cinder_rescs = ["volumes", "snapshots", "gigabytes"]
        for resc in cinder_rescs:
            if resc not in usage:
                row.append("- - -")
                continue
            quota = usage[resc]
            used = usage["%s_used" % resc]
            row.append("%s %s %s" % (used, quota,
                                     self._usage_percent(quota, used)))
        table.add_row(row) 

    def _create(self):
        shared = {}
        tenants, domains = self._tenant_usages()
        for usage in tenants.values():
            if usage["name"].startswith("Personal"):
                self._add_row(self.table["personal"], usage)
            else:
                if usage["domain"] not in shared:
                    inited_table = copy.deepcopy(
                        self.table["shared"]["template"])
                    shared[usage["domain"]] = inited_table
                self._add_row(shared[usage["domain"]], usage)
        for usage in domains.values():
            self._add_row(self.table["domain"], usage)

        self.table["personal"] = self.table["personal"].get_string(
            sortby=self.labels["personal"])
        for domain in sorted(shared.iterkeys()):
            self.table["shared"]["tables"].append(
                shared[domain].get_string(sortby=self.labels["shared"]))
        self.table["domain"] = self.table["domain"].get_string(
            sortby=self.labels["domain"])

    def output(self):
        flag = [0, 1, 1, 1, 1, 1, 1]
        for key, table in self.table.items():
            if key == "shared":
                for v in table["tables"]:
                    self._output(
                        utils.nested_table(v, self._subtable_meta(flag)))
            else:
                self._output(
                    utils.nested_table(table, self._subtable_meta(flag)))
