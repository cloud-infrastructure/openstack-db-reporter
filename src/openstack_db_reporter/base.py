from abc import ABCMeta
from abc import abstractmethod
import prettytable
import traceback


class BaseTable(object):
    __metaclass__ = ABCMeta

    def __init__(self, fields=[]):
        self.fields = fields
        try:
            self._init()
            self._create()
        except Exception as e:
            print e
            print traceback.format_exc()
            self.table = None

    def _init(self):
        self.table = prettytable.PrettyTable(self.fields)
        self.table.align[self.fields[0]] = "l"
        self.table.padding_width = 1

    @abstractmethod
    def _create(self):
        pass

    def _output(self, table):
        if table:
            print table
        else:
            print "no table"

    def output(self):
        self._output(self.table)
